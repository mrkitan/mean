var app = angular.module('testApp');

app.controller('MainCtrl', ['$scope', 'Comment', function ($scope, Comment) {
	$scope.comments = Comment.query({offset: 0, limit: 10});

	$scope.offset = 10;

	$scope.loadMore = function() {
		var moreComments = Comment.query({offset: $scope.offset, limit: 10}, function () {
			var args = [$scope.comments.length, 0].concat(moreComments);
			Array.prototype.splice.apply($scope.comments, args);

			$scope.offset += moreComments.length;
		});
	};

	$scope.newComment = '';

	$scope.addComment = function () {
		var comment = new Comment({text: $scope.newComment});
		comment.$save(function () {
			if( $scope.offset >= $scope.comments.length ) {
				$scope.loadMore();
			}
		});

		$scope.newComment = '';
	};

	$scope.editComment = function (comment) {
		var newText;
		if( (newText = prompt('Enter new variant:', comment.text)) ) {
			Comment.update({_id: comment._id, text: newText}, function () {
				comment.text = newText;
			});
		}
	};

	$scope.removeComment = function (comment) {
		new Comment({_id: comment._id}).$delete(function () {
			$scope.comments.splice( $scope.comments.indexOf(comment), 1 );

			if( $scope.comments.length < 10 ) {
				$scope.loadMore();
			}
		});
	};

	$scope.likeComment = function (comment) {
		Comment.like({_id: comment._id}, function () {
			comment.likes += 1;
		});
	};

	$scope.dislikeComment = function (comment) {
		Comment.dislike({_id: comment._id}, function () {
			comment.likes -= 1;
		});
	};
}]);
