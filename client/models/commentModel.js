
var app = angular.module('testApp');

app.factory('Comment', ['$resource', function ($resource) {
	return $resource('http://localhost:3000/api/comments/:id', { id: '@_id' },
		{
			update: { method:'put' },
			like: {	method: 'put', url: 'http://localhost:3000/api/comments/:id/like'	},
			dislike: {	method: 'put', url: 'http://localhost:3000/api/comments/:id/dislike' }
		});
}]);