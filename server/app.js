var express = require('express');

var bodyParser = require('body-parser');
var cors = require('cors');

var config = require('./config.json');
var comments = require('./controllers/comments');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

app.use('/api/comments', comments);

// catch 404 and forward to error handler
app.use(function(req, res, next){
	res.status(404);
	console.info('%s %d %s', req.method, res.statusCode, req.url);
	res.json({
		error: 'Not found'
	});
});

// error handlers
app.use(function(err, req, res, next){
	res.status(err.status || 500);
	console.error('%s %d %s', req.method, res.statusCode, err.message);
	res.json({
		error: err.message
	});
});

var server = app.listen(config.port, function () {
	console.log('Example app listening at http://localhost:%s', config.port);
});