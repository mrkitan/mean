var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
	text: String,
	date: { type: Date, default: Date.now },
	likes: {type: Number, default: 0}
});

var Model = mongoose.model('comments', schema);

module.exports = Model;