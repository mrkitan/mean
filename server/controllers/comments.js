var express = require('express');
var router = express.Router();

var db = require('../helpers/db');
var Comment = require('../models/comment');

router.get('/', function (req, res) {
	var offset = req.query.offset || 0;
	var limit = req.query.limit || Infinity;

	Comment.find({}).skip(offset).limit(limit).exec(function (err, comments) {
		if( !err ) {
			return res.json(comments);
		} else {
			res.statusCode = 500;
			console.error('Internal error(%d): %s', res.statusCode, err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.post('/', function (req, res) {
	var comment = new Comment({
		text: req.body.text
	});

	comment.save(function (err) {
		if( !err ) {
			console.info("New comment created with id: %s", comment.id);

			return res.json(comment);
		} else {
			res.statusCode = 500;
			console.error('Internal error(%d): %s', res.statusCode, err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.get('/:id', function (req, res) {
	Comment.findById(req.params.id, function (err, comment) {
		if( !comment ) {
			res.statusCode = 404;

			return res.json({
				error: 'Not found'
			});
		}

		if( !err ) {
			return res.json(comment);
		} else {
			res.statusCode = 500;
			console.error('Internal error(%d): %s', res.statusCode, err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.put('/:id', function (req, res) {
	var commentId = req.params.id;

	Comment.findById(commentId, function (err, comment) {
		if( !comment ) {
			res.statusCode = 404;
			console.error('Comment with id: %s Not Found', commentId);
			return res.json({
				error: 'Not found'
			});
		}

		comment.text = req.body.text;

		comment.save(function (err) {
			if( !err ) {
				console.info("Comment with id: %s updated", comment.id);
				return res.json(comment);
			} else {
				res.statusCode = 500;
				console.error('Internal error (%d): %s', res.statusCode, err.message);
				return res.json({
					error: 'Server error'
				});
			}
		});
	});
});

router.delete('/:id', function (req, res) {
	var commentId = req.params.id;

	Comment.findByIdAndRemove(commentId, function (err, comment) {
		if( !err ) {
			console.info("Comment with id: %s removed", commentId);
			return res.sendStatus(204);
		}
		else if( !comment ) {
			res.statusCode = 404;
			console.error('Comment with id: %s Not Found', commentId);
			return res.json({
				error: 'Not found'
			});
		}
		else {
			res.statusCode = 500;
			console.error('Internal error (%d): %s', res.statusCode, err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});


router.put('/:id/like', function (req, res) {
	var commentId = req.params.id;

	Comment.findByIdAndUpdate(commentId, {$inc: {"likes": 1}}, {new: true}, function (err, comment) {
		if( !err ) {
			console.info("Comment with id: %s liked", comment.id);
			return res.json(comment.likes);
		} else if( !comment ) {
			res.statusCode = 404;
			console.error('Comment with id: %s Not Found', commentId);
			return res.json({
				error: 'Not found'
			});
		} else {
			res.statusCode = 500;
			console.error('Internal error (%d): %s', res.statusCode, err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.put('/:id/dislike', function (req, res) {
	var commentId = req.params.id;

	Comment.findByIdAndUpdate(commentId, {$inc: {"likes": -1}}, {new: true}, function (err, comment) {
		if( !err ) {
			console.info("Comment with id: %s disliked", comment.id);
			return res.json(comment.likes);
		} else if( !comment ) {
			res.statusCode = 404;
			console.error('Comment with id: %s Not Found', commentId);
			return res.json({
				error: 'Not found'
			});
		} else {
			res.statusCode = 500;
			console.error('Internal error (%d): %s', res.statusCode, err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});


module.exports = router;