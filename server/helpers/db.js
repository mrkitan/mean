var mongoose = require('mongoose');
var config = require('./../config.json');

mongoose.connect( config.mongoose.uri );

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.on('open', console.info.bind(console, 'Connected to DB.'));

module.exports = mongoose;